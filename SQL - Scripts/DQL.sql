USE Db_Piggy_Controle;
GO
SELECT ID, Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes;

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE ID = 123 AND Ativo != 0;

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Ativo = 1;

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Pontos > 100;

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Pontos < 100;

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE CPF LIKE '21394323612';

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Email LIKE 'gmail%';

SELECT ID,Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Email LIKE REVERSE('%gmail');

SELECT ID,Valor, IdCliente , DataDaCompra FROM Tb_Compras;

SELECT Valor, IdCliente , Nome FROM Tb_Compras INNER JOIN Tb_Clientes ON Tb_Compras.IdCliente = Tb_Clientes.ID;

SELECT Tb_Compras.ID,Valor, IdCliente, DataDaCompra, Nome FROM Tb_Compras INNER JOIN Tb_Clientes ON Tb_Compras.IdCliente = Tb_Clientes.ID WHERE IdCliente = 45;

SELECT Valor, IdCliente ,DataDaCompra ,Nome FROM Tb_Compras INNER JOIN Tb_Clientes ON Tb_Clientes.ID = Tb_Compras.IdCliente WHERE Valor > 100;

SELECT Valor, IdCliente ,DataDaCompra  , Nome FROM Tb_Compras INNER JOIN Tb_Clientes ON Tb_Clientes.ID = Tb_Compras.IdCliente WHERE Valor < 100;

SELECT ID, DataDaPremiacao,IdCliente  FROM Tb_Premios;

SELECT DataDaPremiacao, Nome FROM Tb_Premios INNER JOIN Tb_Clientes ON Tb_Clientes.ID = Tb_Premios.IdCliente;

SELECT DataDaPremiacao, Nome FROM Tb_Premios INNER JOIN Tb_Clientes ON Tb_Clientes.ID = Tb_Premios.IdCliente WHERE DataDaPremiacao = '04/12/20';

