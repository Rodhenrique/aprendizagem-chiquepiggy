﻿using ChiquePiggy.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ChiquePiggy.Domain.Dominios
{
    public class Cliente : DominioBase
    {
        public Cliente()
        {
            Buys = new HashSet<Compra>();
            Awards = new HashSet<Premio>();
        }

        [Required]
        [StringLength(999)]
        public string Id { get; set; }

        [Required]
        [StringLength(999)]
        public string Nome { get; set; }

        [Required]
        [StringLength(11)]
        public string Cpf { get; set; }

        [Required]
        [StringLength(999)]
        public string Email { get; set; }

        [Required]
        public int Pontos { get; set; }

        [Required]
        public bool Ativo { get; set; }
        public virtual ICollection<Compra> Buys { get; set; }
        public virtual ICollection<Premio> Awards { get; set; }

        public Cliente(string nome,string email, string cpf)
        {
            this.Id = Guid.NewGuid().ToString().Replace("-", "").Remove(25);
            this.Nome = nome;
            this.Email = email;
            this.Cpf = cpf;
            this.Cpf = FormatarCpf(true);
            VerificarObjeto();
        }

        protected override void VerificarObjeto()
        {
            string pattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            if (string.IsNullOrEmpty(Email) || !Regex.IsMatch(Email, pattern))
            {
                Erros.Add("Email inválido");
            }

            if (string.IsNullOrEmpty(Nome))
            {
                Erros.Add("Nome inválido");
            }

            if (string.IsNullOrEmpty(Cpf) || Cpf.Length != 11)
            {
                Erros.Add("C.P.F inválido");
            }
        }

        public string FormatarCpf(bool formato)
        {
            if (!string.IsNullOrEmpty(this.Cpf))
            {
                if(formato)
                    return this.Cpf = Cpf.Replace("-", "").Replace(".", "");
                else
                    return Convert.ToUInt64(this.Cpf).ToString(@"000\.000\.000\-00");
            }
            else
            {
                Erros.Add("C.P.F inválido.");
                return "";
            }          
        }

        public bool VerificarPontos()
        {
            return (!string.IsNullOrEmpty(this.Nome) && !string.IsNullOrEmpty(this.Cpf) && !string.IsNullOrEmpty(this.Email) && this.Pontos >= 100);
        }
    }
}
