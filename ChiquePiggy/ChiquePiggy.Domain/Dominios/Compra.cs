﻿using ChiquePiggy.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Domain.Dominios
{
    public class Compra : DominioBase
    {
        public Compra()
        {
        }

        [Required]
        public string Id { get; set; }

        [Required]
        public decimal Valor { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DataDaCompra { get; set; }

        [Required]
        public string IdCliente { get; set; }

        [Required]
        public virtual Cliente Cliente { get; set; }

        public Compra(string idCliente, string valor)
        { 
            this.Id = this.Id = Guid.NewGuid().ToString().Replace("-", "").Remove(25);
            this.Valor = Convert.ToDecimal(valor);
            this.DataDaCompra = DateTime.Now;
            this.IdCliente = idCliente;
            VerificarObjeto();
        }

        protected override void VerificarObjeto()
        {
            if (string.IsNullOrEmpty(IdCliente))
            {
                Erros.Add("ID inválido");
            }

            if (decimal.MinValue == Valor)
            {
                Erros.Add("Valor da compra inválido");
            }
        }
    }
}
