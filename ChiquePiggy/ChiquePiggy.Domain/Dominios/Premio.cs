﻿using ChiquePiggy.Domain.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Domain.Dominios
{
    public class Premio : DominioBase
    {
        public Premio()
        {

        }

        [Required]
        public string Id { get; set; }

        [Required]
        [Column(TypeName = "datetime2")]
        public DateTime DataDaPremiacao { get; set; }

        [Required]
        public string IdCliente { get; set; }

        [Required]
        public virtual Cliente Cliente { get; set; }

        public Premio(string idCliente)
        {
            this.Id = this.Id = Guid.NewGuid().ToString().Replace("-", "").Remove(25);
            this.DataDaPremiacao = DateTime.Now;
            this.IdCliente = idCliente;
            VerificarObjeto();
        }

        protected override void VerificarObjeto()
        {
            if (string.IsNullOrEmpty(IdCliente))
            {
                Erros.Add("ID inválido");
            }
        }
    }
}
