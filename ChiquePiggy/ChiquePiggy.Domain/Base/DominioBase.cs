﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Domain.Base
{
    public abstract class DominioBase
    {
        public DominioBase()
        {
            Erros = new List<string>();
        }
        public List<string> Erros { get; protected set; }
        protected abstract void VerificarObjeto();

        public bool VerificarErros()
        {
            return (Erros.Count == 0);
        }

    }
}
