﻿using ChiquePiggy.Domain.Dominios;
using ChiquePiggy.Service.Base;
using ChiquePiggy.Service.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Servicos
{
    public class PremioServico : ServicoBase,IPremio
    {
        private string ConnectionString { get; } = ConfigurationManager.AppSettings["ConnectionString"];
        private readonly ClienteServico _clienteService;
        public PremioServico(ClienteServico service)
        {
            _clienteService = service;
        }
        public async Task<bool> Deletar(string id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", id, DbType.String);

                    conn.Open();
                    string query = "DELETE Tb_Premios WHERE ID = @id";
                    bool resultado = await conn.ExecuteAsync(sql: query, param: parametros) > 0;
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<IEnumerable<Premio>> Obter()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    string query = "SELECT ID, DataDaPremiacao,IdCliente FROM Tb_Premios;";
                    IEnumerable<Premio> resultado = await (conn.QueryAsync<Premio>(sql: query));
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<Premio> ObterPorId(string id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", id, DbType.String);
                    conn.Open();
                    string query = "SELECT ID, DataDaPremiacao,IdCliente  FROM Tb_Premios WHERE ID = @id;";
                    Premio resultado = await (conn.QueryFirstOrDefaultAsync<Premio>(sql: query, param: parametros));
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<bool> Enviar(string idCliente)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    Premio premio = new Premio(idCliente);
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", premio.Id, DbType.String);
                    parametros.Add("@idCliente", premio.IdCliente, DbType.String);
                    parametros.Add("@dataDaPremiacao", premio.DataDaPremiacao, DbType.DateTime2);

                    conn.Open();
                    string query = "INSERT INTO Tb_Premios([ID],[DataDaPremiacao],[IdCliente]) VALUES(@id,@dataDaPremiacao, @idCliente);";
                    bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                    conn.Close();
                    Erros.AddRange(premio.Erros);
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> Atualizar(Premio premio)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var premioObtido = await ObterPorId(premio.Id);
                    if (premioObtido != null)
                    {
                        var parametros = new DynamicParameters();
                        parametros.Add("@idCliente", (!string.IsNullOrEmpty(premio.IdCliente)) ? premio.IdCliente : premioObtido.IdCliente, DbType.Int32);
                        parametros.Add("@dataDaPremiacao", (premio.DataDaPremiacao != null || premio.DataDaPremiacao != DateTime.MinValue) ? premio.DataDaPremiacao : premioObtido.DataDaPremiacao, DbType.DateTime2);

                        conn.Open();
                        string query = "UPDATE Tb_Premios SET [DataDaPremiacao] = @dataDaPremiacao, [IdCliente] = @idCliente;";
                        bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                        conn.Close();
                        return resultado;
                    }
                    else
                    {
                        Erros.Add("Erro com ID informado.");
                        return false;
                    }
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> VerificarPremio(string cpf)
        {
            try
            {
                int pontos = await _clienteService.ObterPontos(cpf);
                return (pontos >= 100);
            }
            catch
            {
                Erros.Add("Erro com C.P.F informado.");
                return false;
            }
        }

        public async Task<bool> PremiarCliente(Cliente cliente)
        {
            try
            {
                bool resultado = await _clienteService.EnviarPontos(cliente.Cpf, (cliente.Pontos - 100));
                if (resultado)
                    await Enviar(cliente.Id);
                else
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
                
                return true;
            }
            catch 
            {
                Erros.Add("Erro ao conectar no banco de dados.");
                return false;
            }
        }
    }
}
