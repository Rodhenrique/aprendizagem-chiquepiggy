﻿using ChiquePiggy.Domain.Dominios;
using ChiquePiggy.Service.Base;
using ChiquePiggy.Service.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Servicos
{
    public class ClienteServico : ServicoBase, ICliente
    {
        private string ConnectionString { get; } = ConfigurationManager.AppSettings["ConnectionString"];
        public async Task<bool> AtivarCliente(string cpf, bool status)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@cpf", cpf, DbType.String);
                    parameters.Add("@ativo", status, DbType.Boolean);

                    conn.Open();
                    string query = "UPDATE Tb_Clientes SET [Ativo] = @ativo WHERE CPF = @cpf";
                    bool resultado = (await conn.ExecuteAsync(sql: query, param: parameters) > 0);
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> Deletar(string id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", id, DbType.String);
                    conn.Open();
                    string query = "DELETE Tb_Clientes WHERE ID = @id";
                    bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<IEnumerable<Cliente>> Obter()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    string query = "SELECT ID, Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE Ativo != 0;";
                    IEnumerable<Cliente> resultado = await (conn.QueryAsync<Cliente>(sql: query));
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<Cliente> ObterPorCpf(Cliente cliente)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@cpf", cliente.FormatarCpf(true), DbType.String);
                    conn.Open();
                    string query = "SELECT ID, Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE CPF = @cpf";
                    Cliente resultado = await (conn.QueryFirstOrDefaultAsync<Cliente>(sql: query, param: parametros));
                    conn.Close();
                    if (resultado == null)
                        Erros.Add("Usuário não existe no sistema.");
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<Cliente> ObterPorId(string id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", id, DbType.String);
                    conn.Open();
                    string query = "SELECT ID, Nome,CPF, Email, Pontos, Ativo FROM Tb_Clientes WHERE ID = @id AND Ativo != 0;";
                    Cliente cliente = await (conn.QueryFirstOrDefaultAsync<Cliente>(sql: query, param: parametros));
                    conn.Close();
                    return cliente;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<int> ObterPontos(string cpf)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@cpf", cpf, DbType.String);
                    conn.Open();
                    string query = "SELECT ID, Pontos FROM Tb_Clientes WHERE CPF = @cpf AND Ativo != 0;";
                    var result = await (conn.QueryFirstOrDefaultAsync<Cliente>(sql: query, param: parametros));
                    conn.Close();
                    return ((result != null) ? result.Pontos : 0);
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return 0;
                }
            }
        }

        public async Task<bool> Enviar(Cliente novoCliente)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    if (!(await VerifcarClienteAtivo(novoCliente.FormatarCpf(true))))
                    {
                        bool resultado = false;
                        Cliente cliente = new Cliente(novoCliente.Nome,novoCliente.Email, novoCliente.Cpf);
                        if (cliente.VerificarErros())
                        {
                            var parametros = new DynamicParameters();
                            parametros.Add("@id", cliente.Id, DbType.String);
                            parametros.Add("@nome", cliente.Nome, DbType.String);
                            parametros.Add("@cpf", cliente.Cpf, DbType.String);
                            parametros.Add("@email", cliente.Email, DbType.String);
                        
                            conn.Open();
                            string query = "INSERT INTO Tb_Clientes([ID],[Nome],[CPF],[Email]) VALUES(@id,@nome,@cpf,@email)";
                            resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                            conn.Close();
                        }
                        Erros.AddRange(cliente.Erros);
                        return resultado;
                    }
                    else
                    {
                        Erros.Add("Cliente já existe.");
                        return false; 
                    }
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> EnviarPontos(string cpf, int pontos)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@cpf", cpf, DbType.String);
                    parametros.Add("@pontos", pontos, DbType.Int32);

                    conn.Open();
                    string query = @"UPDATE Tb_Clientes SET Pontos = @pontos WHERE CPF = @cpf";
                    bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> Atualizar(string id, Cliente cliente)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    Cliente clienteObtido = await ObterPorId(id);
                    if (clienteObtido != null)
                    {
                        var parametros = new DynamicParameters();
                        parametros.Add("@cpf", (cliente.Cpf != "") ? cliente.Cpf : clienteObtido.Cpf, DbType.String);
                        parametros.Add("@nome", (cliente.Nome != "") ? cliente.Nome : clienteObtido.Nome, DbType.String);
                        parametros.Add("@email", (cliente.Email != "") ? cliente.Email : clienteObtido.Email, DbType.String);
                        parametros.Add("@pontos", (cliente.Pontos >= 0) ? cliente.Pontos : clienteObtido.Pontos, DbType.Int32);

                        conn.Open();
                        string query = @"UPDATE Tb_Clientes SET Nome = @nome, CPF = @cpf, Email = @email, Pontos = @pontos WHERE CPF = @cpf";
                        bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                        conn.Close();
                        return resultado;
                    }
                    else
                    {
                        Erros.Add("Cliente inválido.");
                        return false;
                    }
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<bool> VerifcarClienteAtivo(string cpf)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@Cpf", cpf, DbType.String);
                    conn.Open();
                    string query = "SELECT Ativo FROM Tb_Clientes WHERE CPF = @Cpf;";
                    var resultado = await (conn.QueryFirstOrDefaultAsync<Cliente>(sql: query, param: parametros));
                    conn.Close();
                    if (resultado == null)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return true;
                }
            }
        }
    }
}
