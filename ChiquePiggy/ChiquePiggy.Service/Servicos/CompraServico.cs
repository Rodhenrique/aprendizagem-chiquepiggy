﻿using ChiquePiggy.Domain.Dominios;
using ChiquePiggy.Service.Base;
using ChiquePiggy.Service.Interfaces;
using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Servicos
{
    public class CompraServico : ServicoBase, ICompra
    {
        private string ConnectionString { get; } = ConfigurationManager.AppSettings["ConnectionString"];
        private readonly ClienteServico _clienteService;
        public CompraServico(ClienteServico service)
        {
            _clienteService = service;
        }
        public async Task<bool> Deletar(Compra compra)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", compra.Id, DbType.String);

                    conn.Open();
                    string query = "DELETE Tb_Compras WHERE ID = @id";
                    bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
            }
        }

        public async Task<IEnumerable<Compra>> Obter()
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    conn.Open();
                    string query = "SELECT ID,Valor, IdCliente , DataDaCompra FROM Tb_Compras;";
                    IEnumerable<Compra> resultado = await (conn.QueryAsync<Compra>(sql: query));
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<Compra> ObterPorId(string id)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var parametros = new DynamicParameters();
                    parametros.Add("@id", id, DbType.String);
                    conn.Open();
                    string query = "SELECT ID,Valor, IdCliente , DataDaCompra FROM Tb_Compras WHERE ID = @id;";
                    Compra resultado = await (conn.QueryFirstOrDefaultAsync<Compra>(sql: query, param: parametros));
                    conn.Close();
                    return resultado;
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return null;
                }
            }
        }

        public async Task<bool> Enviar(Compra novaCompra)
        {
            using (var conn = new SqlConnection(ConnectionString))
            {
                try
                {
                    var cliente = await _clienteService.ObterPorCpf(novaCompra.Cliente);
                    if (cliente != null)
                    {
                        bool resultado = false;
                        Compra compra = new Compra(cliente.Id, novaCompra.Valor.ToString());
                        if (compra.VerificarErros())
                        {

                            var parametros = new DynamicParameters();
                            parametros.Add("@id", compra.Id, DbType.String);
                            parametros.Add("@valor", compra.Valor, DbType.Decimal);
                            parametros.Add("@dataDaCompra", compra.DataDaCompra, DbType.DateTime2);
                            parametros.Add("@idCliente", cliente.Id, DbType.String);

                            conn.Open();
                            string query = "INSERT INTO Tb_Compras([ID],[Valor],[DataDaCompra],[IdCliente]) VALUES(@id,@valor, @dataDaCompra, @idCliente);";
                            resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                            if (resultado) { await AtualizarAposCompra(cliente, novaCompra.Valor.ToString()); }
                            conn.Close();
                        }
                        return resultado;
                    }
                    else
                    {
                        Erros.Add("Cliente Inválido.");
                        return false;
                    }
                }
                catch
                {
                    Erros.Add("Erro ao conectar no banco de dados.");
                    return false;
                }
                finally
                {
                    Erros.AddRange(novaCompra.Erros);
                    Erros.AddRange(_clienteService.ColetarErros());

                }
            }
        }

        public async Task<bool> Atualizar(Compra compra)
        {
            try
            {
                Compra compraObtida = await ObterPorId(compra.Id);
                if (string.IsNullOrEmpty(compra.Id) && compraObtida != null)
                {
                    using (var conn = new SqlConnection(ConnectionString))
                    {
                        var parametros = new DynamicParameters();
                        parametros.Add("@id", compra.Id, DbType.String);
                        parametros.Add("@valor", (compra.Valor != 0) ? compra.Valor : compraObtida.Valor, DbType.Decimal);
                        parametros.Add("@dataDaCompra", (compra.DataDaCompra != null) ? compra.DataDaCompra : compraObtida.DataDaCompra, DbType.DateTime2);
                        parametros.Add("@idCliente", (string.IsNullOrEmpty(compra.IdCliente)) ? compra.IdCliente : compraObtida.IdCliente, DbType.String);

                        conn.Open();
                        string query = "UPDATE Tb_Compras SET [Valor] = @valor,[DataDaCompra] = @dataDaCompra,[IdCliente] = @idCliente WHERE ID = @Id;";
                        bool resultado = (await conn.ExecuteAsync(sql: query, param: parametros) > 0);
                        if (!resultado) { throw new Exception("Ocorreu um erro!"); }
                        conn.Close();
                        return resultado;
                    }
                }
                else
                {
                    Erros.Add("Erro com ID solicitado.");
                    return false;
                }
            }
            catch
            {
                Erros.Add("Erro ao conectar no banco de dados.");
                return false;
            }
        }

        public async Task<bool> AtualizarAposCompra(Cliente client, string valor)
        {
            try
            {
                decimal dinheiro = Convert.ToDecimal(valor);
                decimal soma = Math.Round(dinheiro) - dinheiro;
                int pontos = ((soma >= Convert.ToDecimal(" 0.49")) ? 1 : 0) + Convert.ToInt32(Math.Round(dinheiro) + client.Pontos);
                bool resultado = await _clienteService.EnviarPontos(client.Cpf, pontos);

                return resultado;
            }
            catch
            {
                Erros.Add("Erro ao conectar no banco de dados.");
                return false;
            }
        }
    }
}
