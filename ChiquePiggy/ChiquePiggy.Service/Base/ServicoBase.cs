﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Base
{
    public abstract class ServicoBase
    {
        public ServicoBase()
        {
            Erros = new List<string>();
        }
        protected List<string> Erros { get; set; }

        public IEnumerable<string> ColetarErros()
        {
            return Erros;
        }
    }
}
