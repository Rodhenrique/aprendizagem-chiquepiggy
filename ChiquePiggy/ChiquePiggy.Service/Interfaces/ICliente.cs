﻿using ChiquePiggy.Domain.Dominios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Interfaces
{
    public interface ICliente
    {
        Task<IEnumerable<Cliente>> Obter();
        Task<Cliente> ObterPorId(string id);
        Task<bool> Enviar(Cliente cliente);
        Task<bool> Atualizar(string id, Cliente cliente);
        Task<bool> Deletar(string id);
        Task<bool> VerifcarClienteAtivo(string cpf);
        Task<Cliente> ObterPorCpf(Cliente cliente);
        Task<int> ObterPontos(string cpf);
        Task<bool> EnviarPontos(string cpf, int pontos);
        Task<bool> AtivarCliente(string cpf, bool status);
    }
}
