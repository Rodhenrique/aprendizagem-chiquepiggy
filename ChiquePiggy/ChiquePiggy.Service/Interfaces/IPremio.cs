﻿using ChiquePiggy.Domain.Dominios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Interfaces
{
    public interface IPremio
    {
        Task<IEnumerable<Premio>> Obter();
        Task<Premio> ObterPorId(string id);
        Task<bool> Enviar(string idClient);
        Task<bool> Atualizar(Premio premio);
        Task<bool> Deletar(string id);
        Task<bool> VerificarPremio(string cpf);
        Task<bool> PremiarCliente(Cliente cliente);
    }
}
