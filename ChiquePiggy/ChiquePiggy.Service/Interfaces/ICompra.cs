﻿using ChiquePiggy.Domain.Dominios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChiquePiggy.Service.Interfaces
{
    public interface ICompra
    {
        Task<IEnumerable<Compra>> Obter();
        Task<Compra> ObterPorId(string id);
        Task<bool> Enviar(Compra novaCompra);
        Task<bool> Atualizar(Compra compra);
        Task<bool> Deletar(Compra compra);
        Task<bool> AtualizarAposCompra(Cliente cliente, string valor);
    }
}
