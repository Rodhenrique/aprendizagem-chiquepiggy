﻿using ChiquePiggy.Domain.Dominios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChiquePiggy.MVC.ViewModels
{
    public class HomeViewModel
    {
        public CadastroViewModel CadastroViewModel { get; set; }
        public ConsultaViewModel ConsultaViewModel { get; set; }
        public CompraViewModel CompraViewModel { get; set; }
        public PremioViewModel PremioViewModel { get; set; }
        public BuscadorViewModel BuscadorViewModel { get; set; }
        public ClienteViewModel ClienteViewModel { get; set; }
        public CaixaViewModel CaixaViewModel { get; set; }
        public bool Modal { get; set; } = true;
        public List<string> Erros { get; set; } = new List<string>();

        public static HomeViewModel InstanciaHome()
        {
            return new HomeViewModel
            {
                Modal = false,
                CadastroViewModel = new CadastroViewModel(),
                ConsultaViewModel = new ConsultaViewModel(),
                CompraViewModel = new CompraViewModel(),
                PremioViewModel = new PremioViewModel(),
                BuscadorViewModel = new BuscadorViewModel(),
                ClienteViewModel = new ClienteViewModel(),
                CaixaViewModel = new CaixaViewModel()
            };
        }
        public static HomeViewModel InstanciaCadastro(CadastroViewModel cadastroView)
        {
            cadastroView.Cadastro = (cadastroView.Erros.Count == 0 || cadastroView.Erros.Count > 0);

            return new HomeViewModel
            {
                Erros = cadastroView.Erros,
                Modal = true,
                CadastroViewModel = cadastroView,
                ConsultaViewModel = new ConsultaViewModel(),
                CompraViewModel = new CompraViewModel(),
                PremioViewModel = new PremioViewModel()
            };
        }
        public static HomeViewModel InstanciaConsulta(ConsultaViewModel consultaViewModel)
        {
            consultaViewModel.Consulta = (consultaViewModel.Erros.Count == 0 || consultaViewModel.Erros.Count > 0);

            return new HomeViewModel
            {
                Erros = consultaViewModel.Erros,
                Modal = true,
                CadastroViewModel = new CadastroViewModel(),
                ConsultaViewModel = consultaViewModel,
                CompraViewModel = new CompraViewModel(),
                PremioViewModel = new PremioViewModel()
            };
        }
        public static HomeViewModel InstanciaCompra(CompraViewModel compraViewModel)
        {
           compraViewModel.Compra = (compraViewModel.Erros.Count == 0 || compraViewModel.Erros.Count > 0);

            return new HomeViewModel
            {
                Erros = compraViewModel.Erros,
                Modal = true,
                CadastroViewModel = new CadastroViewModel(),
                ConsultaViewModel = new ConsultaViewModel(),
                CompraViewModel = compraViewModel,
                PremioViewModel = new PremioViewModel()
            };
        }
        public static HomeViewModel InstanciaPremio(PremioViewModel premioViewModel)
        {
            premioViewModel.Premio = (premioViewModel.Erros.Count == 0 || premioViewModel.Erros.Count > 0);
            return new HomeViewModel
            {
                Erros = premioViewModel.Erros,
                Modal = true,
                CadastroViewModel = new CadastroViewModel(),
                ConsultaViewModel = new ConsultaViewModel(),
                CompraViewModel = new CompraViewModel(),
                PremioViewModel = premioViewModel
            };
        }
    }

    public class CadastroViewModel
    {
        public List<string> Erros { get; set; } = new List<string>();
        public bool Cadastro { get; set; }
    }

    public class ConsultaViewModel
    {
        public BuscadorViewModel BuscadorViewModel { get; set; } = new BuscadorViewModel();
        public List<string> Erros { get; set; } = new List<string>();
        public Cliente Cliente { get; set; } = new Cliente();
        public bool Consulta { get; set; }
    }

    public class CompraViewModel
    {
        public List<string> Erros { get; set; } = new List<string>();
        public bool Compra { get; set; }
        public bool Premio { get; set; }
    }

    public class PremioViewModel
    {
        public bool Premio { get; set; }
        public List<string> Erros { get; set; } = new List<string>();
        public Cliente Ganhador { get; set; } = new Cliente();
    }
}