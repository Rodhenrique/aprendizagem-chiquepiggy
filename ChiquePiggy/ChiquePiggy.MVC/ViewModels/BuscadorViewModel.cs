﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChiquePiggy.MVC.ViewModels
{
    public class BuscadorViewModel
    {
        [Required(ErrorMessage = "O Cpf é obrigatório!")]
        [StringLength(9, MinimumLength = 9)]
        [RegularExpression(@"([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})", ErrorMessage = "Cpf Inválido")]
        public string Cpf { get; set; }
    }
}