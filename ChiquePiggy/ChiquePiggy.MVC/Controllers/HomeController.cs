﻿using AutoMapper;
using ChiquePiggy.Domain.Dominios;
using ChiquePiggy.MVC.App_Start;
using ChiquePiggy.MVC.ViewModels;
using ChiquePiggy.Service.Servicos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ChiquePiggy.MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ClienteServico _clienteServico;
        private readonly CompraServico _compraServico;
        private readonly PremioServico _premioServico;
        private readonly IMapper _mapperServico;

        public HomeController(ClienteServico service, CompraServico compra, PremioServico premio)
        {
            _clienteServico = service;
            _compraServico = compra;
            _premioServico = premio;
            _mapperServico = AutoMapperConfig.MapperConfiguration().CreateMapper();
        }
        
        [HttpGet]
        public ActionResult Index()
        {
            return View("Index", HomeViewModel.InstanciaHome());
        }

        [HttpPost]
        public async Task<ActionResult> Compra(HomeViewModel homeView)
        {
            CompraViewModel compraView = new CompraViewModel();

            try
            {
                bool resultado = await _compraServico.Enviar(_mapperServico.Map<Compra>(homeView.CaixaViewModel));
                if(resultado)
                {
                    compraView.Premio = await _premioServico.VerificarPremio(homeView.CaixaViewModel.Cpf);
                }

                return View("Index", HomeViewModel.InstanciaCompra(compraView));
            }
            catch 
            {
                return View("Index", HomeViewModel.InstanciaCompra(compraView));
            }
            finally
            {
                compraView.Erros.AddRange(_clienteServico.ColetarErros());
                compraView.Erros.AddRange(_compraServico.ColetarErros().ToList());
            }
        }

        [HttpPost]
        public async Task<ActionResult> Consulta(HomeViewModel homeView)
        {

            ConsultaViewModel consultaView = new ConsultaViewModel();
            try
            {
                consultaView.Cliente = await _clienteServico.ObterPorCpf(_mapperServico.Map<Cliente>(homeView.BuscadorViewModel));
               
                return View("Index", HomeViewModel.InstanciaConsulta(consultaView));
            }
            catch
            {
                return View("Index", HomeViewModel.InstanciaConsulta(consultaView));
            }
            finally
            {
                consultaView.Erros = _clienteServico.ColetarErros().ToList();
            }
        }
        [HttpPost]
        public async Task<ActionResult> Cadastro(HomeViewModel homeView)
        {
            CadastroViewModel cadastroView = new CadastroViewModel();

            try
            {
                bool resultado = await _clienteServico.Enviar(_mapperServico.Map<Cliente>(homeView.ClienteViewModel));
               
                return View("Index", HomeViewModel.InstanciaCadastro(cadastroView));
            }
            catch 
            {
                return View("Index", HomeViewModel.InstanciaCadastro(cadastroView));
            }
            finally
            {
                cadastroView.Erros = _clienteServico.ColetarErros().ToList();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Resgate(BuscadorViewModel buscadorViewModel)
        {
            PremioViewModel premioView = new PremioViewModel();

            try
            {
                Cliente cliente = await _clienteServico.ObterPorCpf(_mapperServico.Map<Cliente>(buscadorViewModel));
                if (cliente.VerificarPontos())
                {
                    bool resultado = await _premioServico.PremiarCliente(cliente);
                }
                
                return View("Index", HomeViewModel.InstanciaPremio(premioView));
            }
            catch
            {
                return View("Index", HomeViewModel.InstanciaPremio(premioView));
            }
            finally
            {
                premioView.Erros = _clienteServico.ColetarErros().ToList();
            }
        }
    }
}