﻿function Mascara(objeto, mascara) {
	obj = objeto
	masc = mascara
	setTimeout("DefinirMascara()", 1)
}
function DefinirMascara() {
	obj.value = masc(obj.value)
}

function MascaraCpf(cpf) {
	cpf = cpf.replace(/\D/g, "")
	cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
	cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2")
	cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
	return cpf
}

function MascaraMoeda(Moeda) {
	Moeda = Moeda.replace('.', '').replace(',', '').replace(/\D/g, '')

	const options = { minimumFractionDigits: 2 }
	const resultado = new Intl.NumberFormat('pt-BR', options).format(parseFloat(Moeda) / 100)
	return resultado
}