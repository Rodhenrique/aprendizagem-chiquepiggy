using AutoMapper;
using ChiquePiggy.MVC.App_Start;
using ChiquePiggy.Service.Interfaces;
using ChiquePiggy.Service.Servicos;
using System.Web.Mvc;
using Unity;
using Unity.Lifetime;
using Unity.Mvc5;

namespace ChiquePiggy.MVC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<ICliente, ClienteServico>(TypeLifetime.Scoped);
            container.RegisterType<ICompra, CompraServico>(TypeLifetime.Scoped);
            container.RegisterType<IPremio, PremioServico>(TypeLifetime.Scoped);

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}