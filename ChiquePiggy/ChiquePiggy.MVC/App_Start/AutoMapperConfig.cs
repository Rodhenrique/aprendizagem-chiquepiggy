﻿using AutoMapper;
using ChiquePiggy.Domain.Dominios;
using ChiquePiggy.MVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChiquePiggy.MVC.App_Start
{
    public static class AutoMapperConfig 
    {
        public static MapperConfiguration MapperConfiguration()
        {
            MapperConfiguration mapperConfiguration = new MapperConfiguration(x => {
                x.CreateMap<ClienteViewModel, Cliente>();
              
                x.CreateMap<BuscadorViewModel, Cliente>()
                .ForMember(dst => dst.Cpf, src => src.MapFrom(e => e.Cpf));

                x.CreateMap<CaixaViewModel, Compra>()
                .ForPath(dst => dst.Cliente.Cpf, src => src.MapFrom(e => e.Cpf));
            });

            return mapperConfiguration;
        }
    }
}